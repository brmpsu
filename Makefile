PRG            = brmpsu
OBJ            = brmpsu.o
MCU_TARGET     = atmega16
OPTIMIZE       = -Os
PROG	       = dragon_jtag
PORT	       = usb

CC             = avr-gcc

override CFLAGS = -g -Wall $(OPTIMIZE) -mmcu=$(MCU_TARGET) $(DEFS) -L/usr/x86_64-pc-linux-gnu/avr/lib

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump

all: hex

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -rf *.o $(PRG).elf $(PRG).hex

hex:  $(PRG).hex

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

install: load

load: $(PRG).hex
	avrdude -p m16 -c $(PROG) -P $(PORT) -U flash:w:$< 

